Social Media and the Transition to College: Functional Variations in Age, Gender and Ethnicity
Tyler J. Horan

Project utilizes TexShop (v3.11), R (v2.15.0)

To recreate research:

1. Run the Required Sweave File : smhighered-r.Rnw

Anonymous datasets are included as csv files